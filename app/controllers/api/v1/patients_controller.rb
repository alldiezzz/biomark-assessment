class Api::V1::PatientsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def create
    render json: ::Api::V1::PatientService.new(patient_params).create
  end

  def patient_params
    params.permit!
  end
end
