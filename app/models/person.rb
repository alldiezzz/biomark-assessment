class Person < ApplicationRecord
  enum gender: ['F', 'M', 'O']
  has_many :clinics, dependent: :destroy
end
