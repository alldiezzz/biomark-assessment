class Clinic < ApplicationRecord
  belongs_to :person

  has_many :diagnoses, dependent: :destroy
end
