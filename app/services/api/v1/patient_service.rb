module Api
  module V1
    class PatientService < Base
      attr_reader :patient_params
      def initialize(patient_params)
        @patient_params = patient_params
      end

      def create
        patient = create_patient
        clinic  = create_clinic(patient)
        create_diagnoses(clinic)

        success_response(patient)
      rescue => error
        failed_response(error)
      end

      def create_patient
        ::Person.create(patient_data)
      end

      def create_clinic(patient)
        ::Clinic.create(clinic_data.merge(person_id: patient.id))
      end

      def create_diagnoses(clinic)
        diagnoses_data.each do |diagnosis_data|
          clinic.diagnoses.create(diagnosis_data)
        end
      end

      def success_response(patient)
        {status: 200, message: "Success create patient data with id: #{patient.id} and id_number: #{patient.id_number}"}
      end

      def failed_response(error)
        {status: 500, message: "Failed create patient data, error: #{error.message}"}
      end
    end
  end
end