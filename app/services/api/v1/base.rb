module Api
  module V1
    class Base
      def patient_data
        if patient_params[:patient_data].present?
          data = patient_params[:patient_data]
        else
          data = map_patient_data
        end
        remap_patient_data(data)
      end

      def map_patient_data
        get_data(patient_keys_name)
      end

      def clinic_data
        get_data(clinic_keys_name)
      end

      def diagnoses_data
        patient_params[:lab_studies].map do |lab_data|
          data = {}
          lab_data.each do |lab|
            data[lab[0]] = lab[1]
          end
          data
        end
      end

      private

      def patient_keys_name
        %w[
          id_number patient_name gender phone_mobile
          date_of_birth first_name last_name
        ]
      end

      def clinic_keys_name
        %w[
          date_of_test lab_number clinic_code
        ]
      end

      def lab_keys_name
        %w[code name value unit ref_range finding result_state]
      end

      def get_data(keys)
        data = {}
        keys.each do |key_name|
          data[key_name] = patient_params[key_name]
        end
        data.with_indifferent_access
      end

      def remap_patient_data(data)
        data = remap_patient_name_data(data)
        data[:date_of_birth] = remap_date_of_birth(data)
        data
      end

      def remap_patient_name_data(data)
        if data[:patient_name].present?
          splited_name = data[:patient_name].split(' ')
          data[:first_name] = splited_name[0]
          data[:last_name] = splited_name[1..splited_name.count].join(' ')
          data.delete(:patient_name)
        end
        data
      end

      def remap_date_of_birth(data)
        Date.parse(data[:date_of_birth])
      rescue
        Date.parse(data[:date_of_birth][0..5] + "01").end_of_month
      end
    end
  end
end