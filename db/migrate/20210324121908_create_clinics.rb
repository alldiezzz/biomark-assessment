class CreateClinics < ActiveRecord::Migration[6.0]
  def change
    create_table :clinics do |t|
      t.datetime    :date_of_test
      t.string      :lab_number
      t.string      :clinic_code
      t.references  :person

      t.timestamps
    end
  end
end
