class CreateDiagnoses < ActiveRecord::Migration[6.0]
  def change
    create_table :diagnoses do |t|
      t.string      :code
      t.string      :name
      t.string      :value
      t.string      :unit
      t.string      :ref_range
      t.string      :finding
      t.string      :result_state
      t.references  :clinic

      t.timestamps
    end
  end
end
