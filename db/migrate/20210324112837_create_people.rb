class CreatePeople < ActiveRecord::Migration[6.0]
  def change
    create_table :people do |t|
      t.string    :id_number
      t.string    :first_name
      t.string    :last_name
      t.integer   :gender
      t.date      :date_of_birth
      t.string    :phone_mobile

      t.timestamps
    end
  end
end
