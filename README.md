# README
Installation:
  - Make sure docker and docker-compose library already installed
  - Run `sudo docker-compose build`
  - After above command done you can run `sudo docker-compose up`

How to run the server:
  - Run `sudo docker-compose up`

Endpoint List:
  - Create Patient Data
    path                    Method
    /api/v1/patients        POST

    Example payload 1:
      {
        "date_of_test": "20210227134300",
        "id_number": "IC000A2",
        "patient_name": "Patient A4",
        "gender": "F",
        "date_of_birth": "19940231",
        "lab_number": "QT196-21-124",
        "clinic_code": "QT196",
        "lab_studies":[
          {
            "code": "2085-9",
            "name": "HDL Cholesterol",
            "value": "cancel",
            "unit": "mg/dL",
            "ref_range": "> 59",
            "finding": "A",
            "result_state": "F"
          }
        ]
      }

    Example payload 2:
      {
        "patient_data":{
          "id_number":"IC000A3",
          "first_name":"Patient",
          "last_name":"A5",
          "phone_mobile":"+6500000000",
          "gender":"M",
          "date_of_birth":"19940231"
        },
        "date_of_test":"20210227134300",
        "lab_number":"QT196-21-124",
        "clinic_code":"QT196",
        "lab_studies":[
          {
          "code":"2085-9",
          "name":"HDL Cholesterol",
          "value":"cancel",
          "unit":"mg/dL",
          "ref_range":"> 59",
          "finding":"A",
          "result_state":"F"
          }
        ]
      }